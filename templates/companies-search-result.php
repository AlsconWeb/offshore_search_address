
<div id="search-app" class="col-xs-12">
    
    <h5>Актуальность базы на: <?php echo get_option('ch_address_xml_date'); ?></h2>
    <br />
    
    <form class="form-horizontal" method="get" action="">
        <input type="search" name="search_company" class="form-control" placeholder="Введите адрес компании"
               value="<?= isset($_GET['search_company']) ? $_GET['search_company'] : '' ?>">
        <input type="submit" value="Поиск">
    </form>
        <?php
        
        if (array_key_exists('search_company', $_GET) && !empty($_GET['search_company'])) {
			global $wpdb;
			$companies_per_page = 20;
			if (!isset($_GET['ch-page'])) $_GET['ch-page'] = 1;
			$start = ($_GET['ch-page'] - 1) * $companies_per_page;
			
			$companyAddr = trim($_GET['search_company']);
			$companyAddr = '+"'.preg_replace('/[\s,]+\s*/i', '" +"', $companyAddr).'"';
			$companyAddr = esc_sql(strtoupper($companyAddr));
			
			$countries = '';
			//$sql = "SELECT * FROM companyHouse where CompanyName LIKE '%$companyName%' OR `RegAddress.AddressLine1` LIKE '%$companyName%' OR `RegAddress.AddressLine2` LIKE '%$companyName%' OR `RegAddress.PostTown` LIKE '%$companyName%' OR `RegAddress.County` LIKE '%$companyName%' OR `RegAddress.Country` LIKE '%$companyName%' LIMIT 100";
			$sql = "SELECT * FROM companyHouse where MATCH (`RegAddress.AddressLine1`, `RegAddress.AddressLine2`,  `RegAddress.PostTown`/*, `RegAddress.Country`*/,  `RegAddress.PostCode`, `RegAddress.PostCode.Imploded`) AGAINST ( '$companyAddr' IN BOOLEAN MODE) $countries LIMIT $start, $companies_per_page";
			$companySearchBody = $wpdb->get_results( $sql );
					
			$sql_tot = "SELECT COUNT(*) as total FROM companyHouse where MATCH (`RegAddress.AddressLine1`, `RegAddress.AddressLine2`,  `RegAddress.PostTown`/*, `RegAddress.Country`*/,  `RegAddress.PostCode`, `RegAddress.PostCode.Imploded`) AGAINST ( '$companyAddr' IN BOOLEAN MODE ) $countries ";
			$companySearchTotoal = $wpdb->get_results( $sql_tot );
			//var_dump($companySearchTotoal);exit;
			
			//var_dump($companySearchBody);exit;
			$exactCompany = $possibleCompanies = array();

			if ($companySearchBody !== null && is_array($companySearchBody)) {		
									
				foreach ($companySearchBody AS $possibleCompany) {
					$thisCompanyDetails = array('name' => (string) $possibleCompany->CompanyName,
				                            'number' => (string) $possibleCompany->CompanyNumber,/*id*/
				                            'address' => (string) trim($possibleCompany->{'RegAddress.AddressLine1'} . ' ' . $possibleCompany->{'RegAddress.AddressLine2'}) . ', ' . $possibleCompany->{'RegAddress.PostTown'} /*. ', ' . $possibleCompany->{'RegAddress.Country'}*/ . ', ' . $possibleCompany->{'RegAddress.PostCode'}, 
				                            'status' => (string) $possibleCompany->CompanyStatus
				                            );
					$possibleCompanies[] = $thisCompanyDetails;
				}
				$companies = array(
			             'match' => $possibleCompanies,
						 'total' => $companySearchTotoal[0]->total);
			}
			
			
	//echo nl2br(print_r($companies, true));
	if (!empty($companies)) {
?>
<h2><?php //echo $title; ?></h2>
<?php
			$companies_list = $companies['match'];
?>
<div class="search-results-list">
	    <h2>Результаты поиска</h2>
        <?php //$pages = floor($data->total_results / 20) ?>
        <h3>Найдено: <?= $companies['total'] ?> </h3>
<ul><!-- id="ch-companies-result"-->
<?php
				foreach ($companies_list as $company) {
?>
	<li><a href="<?php echo CP_Helper::getCompanyDetailsUrl($company['number']); ?>" class="search-result-item"><?php echo $company['name']; ?></a>
			<p class="title"><?php echo $company['status']; ?></p>
           <p class="title"><?php echo $company['address']; ?></p>
    </li>
<?php
				};
?>
</ul>
</div>
        <!--            pagination (start) -->
        <?php if ($companies['total'] > $companies_per_page) : 
			$pages = ceil/*floor*/($companies['total'] / $companies_per_page) ;
        ?>
            <ul class="pagination">

                <?php if (isset($_GET['ch-page']) && $_GET['ch-page'] > 1) : ?>
                    <li class="arrow-left"><a
                                href="<?= '?search_company=' . $_GET['search_company'] . '&ch-page=' . ((int)$_GET['ch-page'] - 1) ?>"></a>
                    </li>
                <?php endif; ?>

                <?php $page = (isset($_GET['ch-page']) && (int)$_GET['ch-page'] > 1) ? $_GET['ch-page'] : 1; ?>
                <?php $page = ($page % 8 == 0) ? $page : (($page - $page % 8) + 1); ?>

                <?php for ($i = $page; $i <= ($page + 8); $i++) : ?>
                    <?php if ((($i-1) * $companies_per_page) <= $companies['total'] /*- $companies_per_page*/) : ?>
                        <?php $current_page = isset($_GET['ch-page']) ? $_GET['ch-page'] : 1; ?>
                        <li class="<?= ($current_page == $i) ? 'active' : '' ?>"><a
                                    href="<?= '?search_company=' . $_GET['search_company'] . '&ch-page=' . $i ?>"><?= $i ?></a>
                        </li>
                    <?php endif; ?>
                <?php endfor; ?>

                <?php if ((((int)$_GET['ch-page'] + 8) * $companies_per_page) <= $companies['total'] - $companies_per_page) : ?>
                    <li class="arrow-right"><a
                                href="<?= '?search_company=' . $_GET['search_company'] . '&ch-page=' . ((int)$_GET['ch-page'] + 1) ?>"></a>
                    </li>
                <?php endif; ?>

            </ul>

            <div class="col-xs-12">
                <form class="form-horizontal pages" method="get" action="#">
                    <input type="hidden" name="search_company" class="form-control"
                           value="<?= isset($_GET['search_company']) ? $_GET['search_company'] : '' ?>">
                    <input type="search" name="ch-page" class="form-control"
                           placeholder="Введите номер страницы: (1 - <?= $pages ?>)">
                    <input type="submit" value="Перейти">
                </form>
            </div>
        <?php endif; ?>
<?php
	} else {
?>
<?php echo sprintf(CP_Helper::getTranslation('No companies found for "%s"'), $_GET['search_company']); ?>
<?php
	}			
			
			
		}
?>
</div> 
<?php
