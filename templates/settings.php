<?
$yes_no = array(
	"0"	=> "No",
	"1"	=> "Yes"
);
$offtypes = array(
	'DIS'	=> 'Disqualified Directors only',
	'LLP'	=> 'Limited Liability Partnerships',
	'CUR'	=> 'Not above',
	'EUR'	=> 'SE and ES appointments only'
);
?>
<div class="wrap">
    <h2>WP Companies House</h2>
    <form method="post" action="options.php"> 
        <?php @settings_fields('ch_search_address-group'); ?>
        <?php @do_settings_fields('ch_search_address', 'ch_search_address-group'); ?>

		<h3></h3>
		Cron команда: <strong>0	0	*	*	*	wget -O /dev/null <?php echo plugins_url('', __FILE__) . '/update.php' ?> > /dev/null 2>&1	</strong>
		<table class="form-table">  
            <tr valign="top">
                <th scope="row"><label for="ch_address_xml_page">Адрес страницы архива с компаниями </label></th>
                <td><input type="text" name="ch_address_xml_page" id="ch_address_xml_page" value="<?php echo get_option('ch_address_xml_page'); ?>" placeholder="" size="80"/></td>
            </tr>
        </table>
        <table class="form-table">  
            <tr valign="top">
                <th scope="row"><label for="ch_address_xml_url_pat">Шаблон для определения ссылки архива</label></th>
                <td><input type="text" name="ch_address_xml_url_pat" id="ch_address_xml_url_pat" value="<?php echo htmlspecialchars(get_option('ch_address_xml_url_pat')); ?>" placeholder="" size="80"/></td>
            </tr>
        </table>
        <table class="form-table">  
            <tr valign="top">
                <th scope="row"><label for="ch_address_xml_date_pat">Шаблон для определения даты архива</label></th>
                <td><input type="text" name="ch_address_xml_date_pat" id="ch_address_xml_date_pat" value="<?php echo get_option('ch_address_xml_date_pat'); ?>" placeholder="" size="80"/></td>
            </tr>
        </table>
        <table class="form-table">  
            <tr valign="top">
                <th scope="row"><label for="ch_address_xml_url">Ссылка на архив с компаниями (для обновления вручную)<!--<br/><small>(слово {DATE} для обозначения даты)</small>--></label></th>
                <td><input type="text" name="ch_address_xml_url" id="ch_address_xml_url" value="<?php echo get_option('ch_address_xml_url'); ?>" placeholder="" size="80"/></td>
            </tr>
        </table>
        <table class="form-table">  
            <tr valign="top">
                <th scope="row"><label for="ch_address_xml_date">Дата актуальной базы (для обновления вручную)</small></label></th>
                <td><input type="text" name="ch_address_xml_date" id="ch_address_xml_date" value="<?php echo get_option('ch_address_xml_date'); ?>" placeholder="" /></td>
            </tr>
        </table>

        <?php @submit_button(); ?>
    </form>
    
    <button class="button button-primary" id="ch_address_ajax_update">Обновить базу</button>
    <div id="ch_address_progress">
		<button class="button button-primary" id="ch_address_ajax_update_stop">Остановить обновление</button>
		<br /><br />
		<div id="ch_address_myProgress">
		  <div id="ch_address_myBar"></div>
		  <div id="ch_address_ajax_update_log"></div>
		</div>
	</div>
</div>
