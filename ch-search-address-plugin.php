<?php
/**
 * Plugin Name: Companies House Address search.
 * Plugin URI: http://mister.ukrweb.net/word/wordpress
 * Description: Companies House Address search.
 * Version: 0.0.10
 * Author: Eugene Babin
 * Author URI: http://freeelancer.com/u/webxtor
 * Text Domain: ch_search_address
 * Domain Path: /locale/
 * License: GPL2
 */

if ( ! class_exists( 'ch_search_address' ) ) {
	class ch_search_address {
		/**
		 * Construct the plugin object
		 */
		public function __construct() {
			$this->define_constants();

			add_shortcode( 'ch_search_address_form', [ __CLASS__, 'ch_search_address_func' ] );

			add_action( 'admin_init', [ &$this, 'admin_init' ] );
			add_action( 'admin_menu', [ &$this, 'add_menu' ] );

			add_action( 'wp_ajax_ch_address_ajax', [ __CLASS__, 'ch_address_ajax_callback' ] );

			add_action( 'wp_enqueue_scripts', [ $this, 'add_script' ] );

		} // END public function __construct

		/**
		 * Add Style and Scripts.
		 */
		public function add_script(): void {
			wp_register_style( 'ch_search_address', WP_PLUGIN_URL . '/ch-search-plugin_2/public/css/main.css', '', '0.0.10', '' );
			wp_enqueue_style( 'ch_search_address' );
		}

		/**
		 * hook into WP's admin_init action hook
		 */
		public function admin_init() {
			// Set up the settings for this plugin
			$this->init_settings();
			// Possibly do additional admin_init tasks
		} // END public static function activate

		/**
		 * Initialize some custom settings
		 */
		public function init_settings() {
			// register the settings for this plugin
			register_setting( 'ch_search_address-group', 'ch_address_xml_page' );
			register_setting( 'ch_search_address-group', 'ch_address_xml_url_pat' );
			register_setting( 'ch_search_address-group', 'ch_address_xml_date_pat' );
			register_setting( 'ch_search_address-group', 'ch_address_xml_url' );
			register_setting( 'ch_search_address-group', 'ch_address_xml_date' );

		} // END public function init_custom_settings()

		/**
		 * add a menu
		 */
		public function add_menu() {
			add_options_page( 'Companies House Address Settings', 'Companies House Address', 'manage_options', 'ch_search_address', [
				&$this,
				'plugin_settings_page',
			] );
		} // END public function add_menu()

		/**
		 * Menu Callback
		 */
		public function plugin_settings_page() {
			if ( ! current_user_can( 'manage_options' ) ) {
				wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
			}

			//add_action('admin_print_scripts', 'my_action_javascript'); // такое подключение будет работать не всегда
			add_action( 'admin_print_footer_scripts', [ __CLASS__, 'ch_address_ajax' ], 99 );
			//add_action( 'wp_ajax_ch_address_ajax', [__CLASS__, 'ch_address_ajax_callback'] );


			// Render the settings template
			include( sprintf( "%s/templates/settings.php", dirname( __FILE__ ) ) );
		} // END public function plugin_settings_page()

		public function define_constants() {
			define( "ch_search_address_PATH", untrailingslashit( plugin_dir_path( __FILE__ ) ) );
		}

		/**
		 * Get the plugin path.
		 *
		 * @return string
		 */
		public function plugin_path() {
			return ch_search_address_PATH;
		}

		static function ch_address_ajax() {
			?>
			<style>
				#ch_address_myProgress {
					width: 100%;
					background-color: #ddd;
				}

				#ch_address_myBar {
					width: 1%;
					height: 30px;
					background-color: #4CAF50;
				}
			</style>
			<script>
				var ch_address_ajax_factor = 0;
				jQuery( document ).ready( function( $ ) {
					$( '#ch_address_progress' ).hide();

					$( '#ch_address_ajax_update' ).click( function( $ ) {
						jQuery( '#ch_address_ajax_update' ).hide();
						jQuery( '#ch_address_progress' ).show();
						ch_address_ajax_factor = 0;

						console.log( 'before iterational call' );
						var interval = 1000;
						( function ch_address_ajax_live() {
							var data = {
								action: 'ch_address_ajax'
							};

							if ( ch_address_ajax_factor == 0 ) {
								data.start = 1;
								ch_address_ajax_factor = 1;
								jQuery( '#ch_address_ajax_update_log' ).html( 'Starting...<br/>' );
							}

							console.log( 'iterational call' );
							jQuery.post( ajaxurl, data, function( response ) {
								console.log( 'iterational call response' );
								updateSatus( response );

								var resp = JSON.parse( response );
								if ( resp.state < 6 ) {
									setTimeout( ch_address_ajax_live, interval * ch_address_ajax_factor );
								}
							} );

							//setTimeout(ch_address_ajax_live, interval * ch_address_ajax_factor);
						} )();

						/*var width = 1;
						 var id = setInterval(frame, 10000);//https://stackoverflow.com/questions/1280263/changing-the-interval-of-setinterval-while-its-running
						 function frame() {
						 if (width >= 100) {
						 clearInterval(id);
						 } else {

						 }
						 }*/
					} );
				} );

				function updateSatus( json ) {
					var response = JSON.parse( json );
					jQuery( '#ch_address_ajax_update_log' ).html( response.log );
					jQuery( '#ch_address_myBar' ).width( response.status + '%' );
					switch ( response.state ) {
						case 1:
							ch_address_ajax_factor = 1;
							break;
					}
				}
			</script>
			<?php
		}

		public function ch_address_ajax_callback() {
			if ( isset( $_POST['start'] ) ) {
				//add_option('ch_address_update_status', 1);

				update_option( 'ch_address_update_log', "Starting." );
				update_option( 'ch_address_update_log_idle', '' );
				$GLOBALS['wp_object_cache']->delete( 'ch_address_update_state', 'options' );
				update_option( 'ch_address_update_state', 0 );
				update_option( 'ch_address_update_found_companies', '' );

				//https://stackoverflow.com/questions/17212747/how-do-i-run-curl-request-in-the-background-in-php

				exec( 'curl "' . plugins_url( '', __FILE__ ) . '/update.php?action=start"' . ' >/dev/null 2>/dev/null &' );
				/*$ch = curl_init();
				 
				curl_setopt($ch, CURLOPT_URL, plugins_url('', __FILE__) . '/update.php');
				curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
				curl_setopt($ch, CURLOPT_TIMEOUT_MS, 700);
				 
				curl_exec($ch);
				curl_close($ch);*/
			}

			$status = 1;// %
			if ( $found_companies = get_option( 'ch_address_update_found_companies' ) ) {
				global $wpdb;
				$sql_tot   = "SELECT AUTO_INCREMENT FROM information_schema.`TABLES` WHERE TABLE_NAME='ch_address_tmp' ";//"SELECT COUNT(*) as total FROM ch_address_tmp ";
				$companyDb = $wpdb->get_var( $sql_tot ) - 1;

				$status = ceil( ( $companyDb / $found_companies ) * 100 );
			}
			update_option( 'ch_address_update_log_idle', get_option( 'ch_address_update_log_idle' ) . '..', false );
			echo json_encode( [
				'log'    => get_option( 'ch_address_update_log' ) . get_option( 'ch_address_update_log_idle' ),
				'state'  => get_option( 'ch_address_update_state' ),
				'status' => $status,
			] );

			wp_die();
		}

		static function ch_search_address_func() {
			global $post;
			$post_slug = $post->post_name;//?

			ob_start();

			include( sprintf( "%s/templates/companies-search-result.php", dirname( __FILE__ ) ) );

			$out = ob_get_contents();
			ob_end_clean();

			return $out;
		}

		public static function downloadDistantFile( $url, $dest ) {
			$options = [
				CURLOPT_FILE           => is_resource( $dest ) ? $dest : fopen( $dest, 'w' ),
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_URL            => $url,
				CURLOPT_FAILONERROR    => true, // HTTP code > 400 will throw curl error
			];

			$ch = curl_init();
			curl_setopt_array( $ch, $options );
			$return = curl_exec( $ch );

			if ( $return === false ) {
				return curl_error( $ch );
			} else {
				return true;
			}
		}

	}

// Installation and uninstallation hooks
	register_activation_hook( __FILE__, [ 'ch_search_address', 'activate' ] );
	register_deactivation_hook( __FILE__, [ 'ch_search_address', 'deactivate' ] );

// instantiate the plugin class
	$ch_search_address = new ch_search_address();
} // EOF if (!class_exists('ch_search_address')) {

// Add a link to the settings page onto the plugin page
if ( isset( $ch_search_address ) ) {
	// Add the settings link to the plugins page
	function ch_search_address_settings_link( $links ) {
		$settings_link = '<a href="options-general.php?page=ch_search_address">Settings</a>';
		array_unshift( $links, $settings_link );

		return $links;
	}

	$plugin = plugin_basename( __FILE__ );
	add_filter( "plugin_action_links_$plugin", 'ch_search_address_settings_link' );
}
?>
