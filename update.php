<?php

ignore_user_abort();
set_time_limit(0);

# No need for the template engine
define( 'WP_USE_THEMES', false );
# Load WordPress Core
// Assuming we're in a subdir: "~/wp-content/plugins/current_dir"
require_once( '../../../wp-load.php' );

//echo get_option('ch_address_update_log') . get_option('ch_address_update_log_idle');exit;
/*update_option('ch_address_update_state', 6);* /
echo get_option('ch_address_update_state'); 
echo get_option('ch_address_update_log'); 
exit();/**/

$page_url = get_option('ch_address_xml_page');
$zip_url = '';
$date = '';
$GLOBALS['wp_object_cache']->delete( 'ch_address_update_state', 'options' );
if (get_option('ch_address_update_state') == 6) { // automated run through cron, 6 state - end of previous run
	
	if ( !$page = file_get_contents($page_url)) {
		ch_address_updateLog("Cannot download $page_url!");
		ch_address_error_exit();
	}
	if (!preg_match('#'.get_option('ch_address_xml_url_pat').'#msi', $page, $m)) {
		ch_address_updateLog('Companies zip URL pattern not matched!');
		ch_address_error_exit();
	}

	// building download link url
	if (preg_match('#^http#i', $m[1])) {
		$zip_url = $m[1];
	} else {
		$zip_url = dirname($page_url) . '/' . $m[1];
	}
	$zip_url;
	$date_pattern = str_replace('Y', '([0-9]+)', get_option('ch_address_xml_date_pat'));
	$date_pattern = str_replace('d', '([0-9]+)', $date_pattern);
	$date_pattern = str_replace('m', '(.+?)', $date_pattern);
	if (!preg_match('#'.$date_pattern.'#msi', $zip_url, $m)) {
		ch_address_updateLog("Date pattern not matched! Using today's date");
		$date = date('Y-m-d');
	} else {
		$date = $m[1] . '-' . $m[2] . '-' . $m[3];
	}
	if ($date == get_option('ch_address_xml_date')) { // no updates
		die('No newer database found.');//exit;
	}
	
	//die('Auto update....');
	
	update_option('ch_address_update_log', "Starting an automated update.");
	update_option('ch_address_update_log_idle', '');
	
} else if (get_option('ch_address_update_state')) { // already running
	if (preg_match('#Automated run started at (.+?)<#i', get_option('ch_address_update_log'), $time)) {
		$hours_passed = (time() - strtotime($time[1])) / (60*60);
		if ($hours_passed > 5) { // more than 5 hours, resetting
			update_option('ch_address_update_state', 6);
		}
	}
	exit;
}

//exit;
				$cfg['tmp_table'] = 'ch_address_tmp';
				$cfg['target_table'] = 'companyHouse';

				$url = $zip_url ? $zip_url : get_option('ch_address_xml_url');
				if ($zip_url) {
					update_option('ch_address_update_log', 'Automated run started at ' . date('Y-m-d H:i:s') );
					update_option('ch_address_update_log_idle', '');
				}
				ch_address_updateLog('Downloading ' . $url);
				update_option('ch_address_update_state', 1); // downloading
				$files_dir = plugin_dir_path(__FILE__) . 'files/';
				$local_file = $files_dir . basename($url);
				if (!file_exists($local_file)) {
					$res = ch_search_address::downloadDistantFile($url, $local_file);
					if ($res === true) {
						ch_address_updateLog('Done downloading!');
					} else {
						ch_address_updateLog('Download error: ' . $res . 'Exiting');
						ch_address_error_exit();
					}
					
				} else {
						ch_address_updateLog('The file being already downloaded!');
				}
				//update_option('ch_address_update_log', get_option('ch_address_update_log') . 'Unzipping file.<br/>');

				update_option('ch_address_update_state', 2); // extracting
				$zip = new ZipArchive;
				$res = $zip->open($local_file);
				if ($res === TRUE) {
					for( $i = 0; $i < $zip->numFiles; $i++ ){ 
						$csv_file = $zip->statIndex( $i ); //print_r($csv_file);
						$csv_file = $csv_file['name'];
						ch_address_updateLog('Found file ' . $csv_file );
						//break;
					}//exit;
					
					$csv_file = $files_dir . $csv_file;
					
					if (!file_exists($csv_file)) {
						ch_address_updateLog('Extracting');
						$zip->extractTo(dirname($local_file) . '/');
						$zip->close();
						ch_address_updateLog('Extracted');
					} else {
						ch_address_updateLog('File already extracted');
					}
				} else {
				  	ch_address_updateLog('Extraction error: ' . $res . 'Exiting');
					ch_address_error_exit();
				}
				
				ch_address_updateLog('Counting companies');
				$file=$csv_file;
				$linecount = -1;
				$handle = fopen($file, "r");
				while(!feof($handle)){
					$line = fgets($handle);
					$linecount++;
				}
				fclose($handle);
				ch_address_updateLog('Found companies: ' . $linecount);
				update_option('ch_address_update_found_companies', $linecount);
				
				//sleep(30);
				//update_option('ch_address_update_log', get_option('ch_address_update_log') . 'Done!');
				
				update_option('ch_address_update_state', 3); // SQL import
				//ch_address_updateLog('Starting SQL import.');
				
				global $wpdb;
				
				$sql = "CREATE TABLE IF NOT EXISTS `{$cfg['tmp_table']}` (
				  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
				  `CompanyNumber` varchar(150) DEFAULT NULL,
				  `CompanyName` varchar(255) DEFAULT NULL,
				  `RegAddress.AddressLine1` varchar(255) DEFAULT NULL,
				  `RegAddress.AddressLine2` varchar(255) DEFAULT NULL,
				  `RegAddress.PostTown` varchar(150) DEFAULT NULL,
				  `RegAddress.County` varchar(150) DEFAULT NULL,
				  `RegAddress.Country` varchar(150) DEFAULT NULL,
				  `RegAddress.PostCode` varchar(100) DEFAULT NULL,
				  `RegAddress.PostCode.Imploded` varchar(100) DEFAULT NULL,
				  `CompanyStatus` varchar(100) DEFAULT NULL,
				  UNIQUE KEY `id` (`id`)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8;";//UNSIGNED smallint(4)
				$wpdb->query($sql);
				ch_address_check_sql_error();
				
				$sql = "TRUNCATE TABLE `{$cfg['tmp_table']}`";
				$wpdb->query($sql);
				ch_address_check_sql_error();
				
				$sql = "SHOW INDEX FROM `{$cfg['tmp_table']}` WHERE KEY_NAME = 'RegAddress.AddressLine1';";
				if ($wpdb->get_var( $sql ) ) {
					$sql = "DROP INDEX `RegAddress.AddressLine1` ON `{$cfg['tmp_table']}`;";
					$wpdb->query($sql);
				}
				ch_address_check_sql_error();
				
				ch_address_updateLog('Prepared tmp table. Starting import.');
								
				$sql = "LOAD DATA LOCAL INFILE '$csv_file' INTO TABLE `{$cfg['tmp_table']}` CHARACTER SET 'utf8' FIELDS TERMINATED BY ',' ENCLOSED BY '\"' ESCAPED BY '' LINES TERMINATED BY '\n' IGNORE 1 LINES 
				(CompanyName, `CompanyNumber`, @dummy, @dummy, `RegAddress.AddressLine1`, `RegAddress.AddressLine2`,`RegAddress.PostTown`, 
				`RegAddress.County`, `RegAddress.Country`, `RegAddress.PostCode`, @dummy, `CompanyStatus`) SET `RegAddress.PostCode.Imploded` = REPLACE(`RegAddress.PostCode`, ' ', '')"; 
				//$wpdb->query($sql);

				$sql_cmd = str_replace('"', '\"', str_replace('`', '\`', $sql));
				$cmd = 'mysql -u ' . DB_USER . ' -p' . DB_PASSWORD . ' -e "USE ' . DB_NAME . '; ' . $sql_cmd  . ';"  >/dev/null  2>&1 ';
				exec($cmd, $output, $return_var);
				//print_r($output);
				//ch_address_updateLog("mysql import status: $return_var, output: " . implode(',', $output));
				

				
				ch_address_updateLog('Import done! Creating indexes.');
				update_option('ch_address_update_state', 4); // Indexes 
				
				$sql = "ALTER TABLE `{$cfg['tmp_table']}` ADD FULLTEXT KEY `RegAddress.AddressLine1` (`RegAddress.AddressLine1`,`RegAddress.AddressLine2`,`RegAddress.PostTown`,`RegAddress.PostCode`, `RegAddress.PostCode.Imploded`)";
				$wpdb->query($sql);
				ch_address_check_sql_error();
				
				ch_address_updateLog('Indexes created. Replacing live table.');
				update_option('ch_address_update_state', 5); // Tables replacement 
				
				$backup_table = "{$cfg['target_table']}_" . date('Y_m_d_H_i_s');
				$sql = "RENAME TABLE `{$cfg['target_table']}` TO `$backup_table`,  `{$cfg['tmp_table']}` TO `{$cfg['target_table']}`;";
				$wpdb->query($sql);
				ch_address_check_sql_error();
				
				$sql = "DROP TABLE `$backup_table`;";
				$wpdb->query($sql);
				ch_address_check_sql_error();
				
				unlink($csv_file);
				unlink($local_file);
				
				if ($date) {
					update_option('ch_address_xml_date', $date); 
				}
				$GLOBALS['wp_object_cache']->delete( 'ch_address_update_log', 'options' );
				ch_address_updateLog('ALL DONE!');
				sleep(1);
				update_option('ch_address_update_state', 6); 


function ch_address_updateLog($message) {
		$GLOBALS['wp_object_cache']->delete( 'ch_address_update_log_idle', 'options' );
		update_option('ch_address_update_log', get_option('ch_address_update_log') . get_option('ch_address_update_log_idle')  . "<br/>\r\n". $message);
		update_option('ch_address_update_log_idle', '', false);
		echo $message;
}

function ch_address_check_sql_error() {
	global $wpdb;
	
    if($wpdb->last_error !== '') :

        $str   = htmlspecialchars( $wpdb->last_error, ENT_QUOTES );
        $query = htmlspecialchars( $wpdb->last_query, ENT_QUOTES );
		
		/*$message = "<div id='error'>
        <p class='wpdberror'><strong>WordPress database error:</strong> [$str]<br />
        <code>$query</code></p>
        </div>";*/
        $message = "Database error: [$str]<br />\r\nQuery:$query<br/>\r\n";
        ch_address_updateLog( $message);
        ch_address_error_exit();

    endif;
}

function ch_address_error_exit() {
	$email = get_option( 'admin_email' );
	$subject = 'Ошибка автоматического обновления базы поиска по адресам компаний ВБ';
	mail($email, $subject, get_option('ch_address_update_log'));
	exit();
}
